(use gl glu glut soil)

(define user-rotate-x (make-parameter 0))
(define user-rotate-y (make-parameter 0))
(define user-rotate-z (make-parameter 0))
(define texture-id    (make-parameter 0))

; create a new texture using the file provided as bitmap data source
(define (load-pic filename)
  (soil:load_OGL_texture 
    filename 
    soil:LOAD_AUTO
    soil:CREATE_NEW_ID
    (bitwise-ior soil:FLAG_POWER_OF_TWO soil:FLAG_INVERT_Y)))

(define (render-pic id)
  (gl:Color3f 1.0 1.0 1.0)
  (gl:PolygonMode gl:FRONT_AND_BACK gl:FILL)
  (gl:Enable gl:TEXTURE_2D)
  (gl:BindTexture gl:TEXTURE_2D id)
  (gl:Begin gl:QUADS)
    (gl:TexCoord2f 0.0 1.0)
    (gl:Vertex2f   0.0 300)
    (gl:TexCoord2f 1.0 1.0)
    (gl:Vertex2f   300 300)
    (gl:TexCoord2f 1.0 0.0)
    (gl:Vertex2f   300 0.0)
    (gl:TexCoord2f 0.0 0.0)
    (gl:Vertex2f   0.0 0.0)
  (gl:End)
  (gl:Disable gl:TEXTURE_2D))

(define (render)
  (gl:PushMatrix)
    (gl:Clear (bitwise-ior gl:COLOR_BUFFER_BIT gl:DEPTH_BUFFER_BIT))
    (gl:PolygonMode gl:FRONT_AND_BACK gl:LINE)
    (gl:Enable gl:POLYGON_SMOOTH)
    (gl:Rotatef (user-rotate-x) 1.0 0.0 0.0)
    (gl:Rotatef (user-rotate-y) 0.0 1.0 0.0)
    ;(gl:Rotatef (user-rotate-z) 0.0 0.0 1.0)
    (gl:Translatef -150 -150 0)
    (render-pic (texture-id))
  (gl:PopMatrix)
  (glut:SwapBuffers))

(define (reshape w h)
  (gl:Viewport 0 0 w h)
  (gl:MatrixMode gl:PROJECTION)
  (gl:LoadIdentity)
  (glu:Perspective 60.0 (/ w h) 1.0 5000.0)
  (gl:MatrixMode gl:MODELVIEW)
  (gl:LoadIdentity)
  (glu:LookAt 0.0 0.0 512.0 0.0 0.0 0.0 0.0 1.0 0.0))

(define (idle)
  (user-rotate-x (+ (user-rotate-x) 1.0))
  (user-rotate-y (+ (user-rotate-y) 1.0))
  (user-rotate-z (+ (user-rotate-z) 1.0))
  (glut:PostRedisplay))

(define (key-down k x y)
  (case (char->integer k)
    ((27) (exit))
    (else (begin (display (char->integer k))(newline)))))

(glut:InitWindowSize 640 480)
(glut:CreateWindow "SOIL test")

(glut:KeyboardFunc key-down)
(glut:DisplayFunc render)
(glut:ReshapeFunc reshape)
(glut:IdleFunc idle)

(texture-id 
  (load-pic "chicken-small.png"))

(glut:MainLoop)

